<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Frutas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Chilanka|Roboto&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        .content {
            text-align: center;
            font-family: 'Chilanka'
        }
            .card {
                width: 100%;
                max-width: 400px;
                border: 1px solid;
                border-radius: 5px;
                margin: 20px auto;
            }
            .card-header {
                padding: 20px;
                text-align: center;
                font-size: 25px;
                color: #fff;
            }
            .card-body img {
                width: 100%;
                height: 100%;
                object-fit: cover;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                @foreach ($fruits as $fruit)
                    <div class="card" style="border-color: {{ $fruit->color }}"> 
                        <div class="card-header" style="background: {{ $fruit->color }};">{{ $fruit->name }}</div>
                        <div class="card-body"><img src="{{ $fruit->image }}"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
</html>
