<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiFruitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            [
                "name" => "Plátano",
                "color" => "#ffd633",
                "image" => "https://cdn.pixabay.com/photo/2016/07/08/18/05/banana-1504956_960_720.png"
            ],
            [
                "name" => "Manzana",
                "color" => "#ff0000",
                "image" => "https://cdn.pixabay.com/photo/2017/09/10/18/11/apple-2736410_960_720.png"
            ],
            [
                "name" => "Naranja",
                "color" => "#ff751a",
                "image" => "https://cdn.pixabay.com/photo/2014/08/26/15/25/oranges-428072_960_720.jpg"
            ],
            [
                "name" => "Pera",
                "color" => "#00b33c",
                "image" => "https://cdn.pixabay.com/photo/2010/12/13/10/06/food-2280_960_720.jpg"
            ]
        ];

        return json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
